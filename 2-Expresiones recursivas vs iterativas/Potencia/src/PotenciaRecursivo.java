import java.util.Scanner;

public class PotenciaRecursivo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner lector = new Scanner(System.in);
		System.out.println("Hallar la potencia de un numero (MODO RECURSIVO)");
		System.out.println("\nDigite la base: ");
		int base = lector.nextInt();
		System.out.println("Digite la potencia: ");
		int potencia = lector.nextInt();
		
		System.out.println("El resultado es: "+potencia(base, potencia));
	}
	
	public static int potencia(int n, int m) {
		
		if(m == 0)
			return 1;
		
		if(m == 1)
			return n;
		
		return potencia(n, m-1)*n;
	}

}
