
public class FiboRecursiva {

	public static void main(String[] args) {

		//SERIE:  1 1 2 3 5 8 13 21 34 ...
		 
		int n = 9 ;
		
		System.out.println(fibo(n-1));
		
	}
	
	static int fibo(int n) {
		
		if (n < 2 ) {
			return 1;
		}else {
			return fibo(n-1)+fibo(n-2);
		}
		
	
	}

}
