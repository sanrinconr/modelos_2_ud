
public class FiboIterativa {

	public static void main(String[] args) {

		//SERIE:  1 1 2 3 5 8 13 21 34 ...

		int n = 9;
		System.out.println(fibo(n));

	}
	
	static int fibo(int n) {
		
		int a=1;
		int b=1;
		int res=a+b;
		
		for (int i = 3 ; i<n ; i++) {	
			a=b;
			b=res;
			res = a+b;
		}
		return res;
	}

}
