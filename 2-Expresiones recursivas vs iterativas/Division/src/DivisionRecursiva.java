import java.util.Scanner;

public class DivisionRecursiva {

	public static void main(String[] args) {
		
		Scanner lector = new Scanner(System.in);
		System.out.println("Hallar la division de un numero (MODO RECURSIVO)");
		System.out.println("\nDigite el numerador: ");
		int numerador = lector.nextInt();
		System.out.println("Digite el denominador: ");
		int denominador = lector.nextInt();

		System.out.println("El resultado es: " + dividir(numerador, denominador));
	}

	public static int dividir(int a, int b) {

		if (a >= b)
			return 1 + dividir(a - b, b);

		else 
			return 0;
	}

}
