sumar::[Int]->Int
sumar [ ] = 0
sumar (x:xs) = x + sumar(xs)

promedio::[Int]->Double
promedio x =   fromIntegral (sumar(x)) / fromIntegral (length x)

mayor::[Int]->Int
mayor [x] = x
mayor (x:xs) = if x < mayor(xs)
    then x
    else mayor(xs)

cercano::[Int] -> Double-> Int
cercano [x] prom =x
cercano (x:xs) prom = if (round prom) ==  x
    then x
    else if round (prom / fromIntegral x ) == 1
      then x
      else cercano xs prom

lista::[[Int]]->[Int]
lista x = [ cercano r (promedio r) | r<-x]
