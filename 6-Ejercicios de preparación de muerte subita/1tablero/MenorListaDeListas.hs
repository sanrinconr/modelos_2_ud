-- Dada una lista de listas generar una lista con el menor elemento de cada sublista

menor::[Int]->Int
menor [x] = x
menor (x:xs) = if x < menor(xs)
    then x
    else menor(xs)


listaMenores::[[Int]]->[Int]
listaMenores m = [menor x | x <- m]
