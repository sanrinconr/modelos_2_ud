
multiplos::Int->[Int]->Int
multiplos x [] = 0
multiplos x (a:as)= if (mod a x) == 0 && a/=x
    then 1 + multiplos x as
    else multiplos x as
divisores::Int->[Int]->Int
divisores x [] = 0
divisores x (a:as)= if (mod x a) == 0 && a/=x
      then 1 + divisores x as
      else divisores x as

menor::[Int]->Int
menor [x] = x
menor (x:xs) = if x < menor(xs)
    then x
    else menor(xs)

mayor::[Int]->Int
mayor [x] = x
mayor (x:xs) = if x > mayor(xs)
    then x
    else mayor(xs)


tupla::[[Int]]->[[Int]]
tupla x = [[menor r , mayor r , multiplos (menor r) r, divisores (mayor r) r] | r<-x]
