sumar::[Int]->Int
sumar [ ] = 0
sumar (x:xs) = x + sumar(xs)

mayor::[Int]->Int
mayor [x] = x
mayor (x:xs) = if x > mayor(xs)
    then x
    else mayor(xs)

menor::[Int]->Int
menor [x] = x
menor (x:xs) = if x < menor(xs)
  then x
  else menor(xs)

lista::[[Int]]->[(Int , Int , Int)]
lista x = [(menor r , mayor r ,  ((menor r) + (mayor r)) ) | r <- x]
