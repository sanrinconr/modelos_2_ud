import java.text.DecimalFormat;
import java.util.Scanner;

//JOSUE NU�EZ 20172020071	
//SANTIAGO RINCON 20172020084
public class Funciones {

	public static void main(String[] args) {
		System.out.println("******CALCULO DE OPERACIONES BASICAS CON FUNCIONES********");
		Scanner myObj = new Scanner(System.in);  // Create a Scanner object
	    System.out.println("Ingrese el numero 1: ");
	    int a = myObj.nextInt();  // Read user input
	    System.out.println("Ingrese el numero 2: ");
	    int b = myObj.nextInt();  // Read user input
		
		System.out.println("Suma:           " +a +" + "+ b+ " = "+ suma(a,b));
		System.out.println("Resta:          " +a +" - "+ b+ " = "+ resta(a,b));
		System.out.println("Multiplicacion: " +a +" * "+ b+ " = "+ multiplica(a,b));
		System.out.println("Division:       " +a +" / "+ b+ " = "+ divide(a,b));
		
	}
	
	static int suma(int a , int b) {
		return a+b;
	}
	
	static int resta(int a , int b) {
		return a-b;
	}
	static int multiplica(int a , int b) {
		return a*b;
	}
	static double divide(double a , double b) {
		double division = a/b;
		double res = Math.round(division * 10000.0) / 10000.0;
		return res;
	}

}
